import React, { Component } from 'react';
import './App.css';
import Flat from './components/flat';
 
class App extends Component {

constructor(props){
  super(props);
  this.state ={
    flats:[]
  };
}


//Set the state of the app
componentDidMount(){
  const url  = 'https://raw.githubusercontent.com/lewagon/flats-boilerplate/master/flats.json';
  fetch(url)
    .then(response => response.json()) //  A tweak in order to convert the raw file to a JSON
    .then((data) => {
      this.setState({
          flats: data 
      });
    })
}

  render() {
    return (
      <div>
        <div className="app">
          <div className="main">
              <div className="search"></div>
              <div className="flats">
              {this.state.flats.map((flat) =>{
                return <Flat flat={flat}/>
              })}
              </div>
          </div>
          <div className="map"></div>

        </div>
      </div>
    );
  }
}

export default App;
